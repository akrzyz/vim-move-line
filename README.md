### What is it? ###

Allow to move lines in simple way.

By default lines are moved with alt+arrows in normal, input and visual mode.

### Configuration ###
Key mapping may be changed by setting variables:

*g:move_line_up_map*

*g:move_line_down_map* 

*g:move_line_left_map*

*g:move_line_right_map*