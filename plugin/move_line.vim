"key mapping
if !exists('g:move_line_up_map')    | let g:move_line_up_map    = '<A-UP>'    | endif
if !exists('g:move_line_down_map')  | let g:move_line_down_map  = '<A-DOWN>'  | endif
if !exists('g:move_line_left_map')  | let g:move_line_left_map  = '<A-LEFT>'  | endif
if !exists('g:move_line_right_map') | let g:move_line_right_map = '<A-RIGHT>' | endif

"move lines down
execute "nnoremap <silent>" . g:move_line_down_map . " :m+<CR>"
execute "inoremap <silent>" . g:move_line_down_map . " <Esc>:m+<CR>gi"
execute "vnoremap <silent>" . g:move_line_down_map . " :m'>+<CR>gv"
"move lines up
execute "nnoremap <silent>" . move_line_up_map . " :m-2<CR>"
execute "inoremap <silent>" . move_line_up_map . " <Esc>:m-2<CR>gi"
execute "vnoremap <silent>" . move_line_up_map . " :m-2<CR>gv"
"move lines left
execute "nnoremap <silent>" . g:move_line_left_map . " <<"
execute "inoremap <silent>" . g:move_line_left_map . " <Esc><<gi"
execute "vnoremap <silent>" . g:move_line_left_map . " <gv"
"move lines right
execute "nnoremap <silent>" . g:move_line_right_map . " >>"
execute "inoremap <silent>" . g:move_line_right_map . " <Esc>>>gi"
execute "vnoremap <silent>" . g:move_line_right_map . " >gv"

